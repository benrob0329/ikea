<div align="center">

# The Infinite IKEA

![screenshot](screenshot.png)

[![ContentDB](https://content.minetest.net/packages/benrob0329/ikea/shields/downloads/)](https://content.minetest.net/packages/benrob0329/ikea/)
[![license](https://img.shields.io/badge/license-MIT-blue)](https://gitlab.com/benrob0329/ikea/-/blob/master/LICENSE)

### A survival horror game for the [Minetest](https://www.minetest.net) Game Engine

You wake up, surrounded by shelving and concrete.<br>
You must fight to survive in the wandering aisles of this emporious realm
(based on [SCP-3008](http://www.scp-wiki.net/scp-3008)).

</div>

* Testbench Server: `vps.totallynotashadyweb.site:30000`
* [IRC](ircs://irc.libera.chat:6697/##minetest-ikea)
* [Development Video Series](https://www.youtube.com/playlist?list=PL6peF9H3xBeruiqFhIRk58t5cluDsnoaP)

If you would like to contribute to the project, take a look at:

* [Contribution Guidelines](CONTRIBUTING.md)
* [Issue Tracker](https://gitlab.com/benrob0329/ikea/issues)
* [Milestones](https://gitlab.com/benrob0329/ikea/-/milestones)

**This project is not, and has never been associated with Inter IKEA Systems B.V.**
