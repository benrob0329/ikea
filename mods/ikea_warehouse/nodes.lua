minetest.register_node("ikea_warehouse:floor", {
	description = "Base floor node (you hacker you!)",
	paramtype = "light",
	tiles = {
		{ name = "ikea_warehouse_concrete.png", scale = 16, align_style = "world" },
	},
	is_ground_content = true,
	sunlight_propagates = true,
	groups = { static = 1 },
})

minetest.register_node(
	"ikea_warehouse:rack",
	table.merge(ikea.default_mesh(), {
		description = "Racks, or big metal shelves that hold boxes (you hacker you!)",
		mesh = "ikea_warehouse_rack.obj",
		tiles = { name = "ikea_warehouse_rack.png" },
		paramtype2 = "none",
		is_ground_content = true,
		sunlight_propagates = true,
		groups = { static = 1 },
	})
)

minetest.register_node("ikea_warehouse:lamp_light", {
	description = "Lights That Light The Warehouse",
	drawtype = "airlike",
	paramtype = "light",
	light_source = minetest.LIGHT_MAX,
	walkable = false,
	pointable = false,
	groups = { static = 1, ikea_light = 1, ikea_fx_dust = 1 },
	ikea_light_swap_node = "ikea_warehouse:lamp_light_off",
	ikea_fx_dust_amount = 1,
	ikea_fx_dust_spawner_chance = 1,
})

minetest.register_node("ikea_warehouse:lamp_light_off", {
	description = "Lights That Light The Warehouse (off)",
	drawtype = "airlike",
	paramtype = "light",
	walkable = false,
	pointable = false,
	ikea_light_swap_node = "ikea_warehouse:lamp_light",
	groups = { static = 1, ikea_light = 2 },
})

minetest.register_node(
	"ikea_warehouse:lamp",
	table.merge(ikea.default_mesh(), {
		description = "Lights That Light The Warehouse",
		mesh = "ikea_warehouse_light.obj",
		tiles = { { name = "ikea_warehouse_light.png" } },
		walkable = false,
		pointable = false,
		groups = { static = 1 },
	})
)

minetest.register_node("ikea_warehouse:row_sign", {
	description = 'Signs To "Mark" Each Row',
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = { { -4 / 16, -8 / 16, -4 / 16, 4 / 16, 8 / 16, 4 / 16 } },
	},
	tiles = {
		{ name = "ikea_warehouse_row_sign_top.png" },
		{ name = "ikea_warehouse_row_sign_top.png" },
		{
			name = "ikea_warehouse_row_sign_side.png",
			scale = 16,
			align_style = "world",
		},
		{
			name = "ikea_warehouse_row_sign_side.png",
			scale = 16,
			align_style = "world",
		},
		{
			name = "ikea_warehouse_row_sign_side.png",
			scale = 16,
			align_style = "world",
		},
		{
			name = "ikea_warehouse_row_sign_side.png",
			scale = 16,
			align_style = "world",
		},
	},
	paramtype = "light",
	walkable = false,
	pointable = false,
	is_ground_content = true,
	sunlight_propagates = true,
	groups = { static = 1 },
})

minetest.register_node("ikea_warehouse:box", {
	description = "Cardboard Box",
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {
			{ -7.999 / 16, -7.999 / 16, -7.999 / 16, 7.999 / 16, 7.999 / 16, 7.999 / 16 },
		},
	},
	tiles = {
		{ name = "ikea_warehouse_box_top.png", backface_culling = false },
		{ name = "ikea_warehouse_box_bottom.png", backface_culling = false },
		{ name = "ikea_warehouse_box_side.png", backface_culling = false },
		{ name = "ikea_warehouse_box_side.png", backface_culling = false },
		{ name = "ikea_warehouse_box_front.png", backface_culling = false },
		{ name = "ikea_warehouse_box_front.png", backface_culling = false },
	},
	paramtype = "light",
	paramtype2 = "facedir",
	use_texture_alpha = "clip",
	groups = { falling_node = 1, carryable = 1, facsimile = 1 },
})
