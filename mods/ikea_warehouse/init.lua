-- Files --
local modpath = minetest.get_modpath("ikea_warehouse")
dofile(modpath .. "/nodes.lua")

-- Local Aliases --
local bound_perlin = ikea_mapgen.bound_perlin
local fill_vmanip_area = ikea_mapgen.fill_vmanip_area
local v_new = vector.new
local get_content_id = minetest.get_content_id

local facsimiles = ikea_facsimile.facsimiles

-- Settings --
local should_place_aisle = ikea_mapgen.every_n_mapblocks(4)
local Perlin = PerlinNoise({
	offset = 0,
	scale = 15,
	spread = v_new(10, 10, 10),
	seed = 1,
	octaves = 2,
	persistence = 3,
	lacunarity = 2,
	flags = "defaults, absvalue",
})

-- Content IDs --
local c_iw = get_content_id("ikea:invisible_wall")
local c_floor = get_content_id("ikea_warehouse:floor")
local c_rack = get_content_id("ikea_warehouse:rack")
local c_lamp = get_content_id("ikea_warehouse:lamp")
local c_lamp_light = get_content_id("ikea_warehouse:lamp_light")
local c_sign = get_content_id("ikea_warehouse:row_sign")

-- We need to find these after everything else has been registered
local applicable_furniture, c_box
minetest.after(0, function()
	applicable_furniture = ikea.get_nodes_by_groups({ spawn_on_rack = 1 })
	c_box = get_content_id(ikea_facsimile.facsimiles["ikea_warehouse:box"])
end)

-- Mapgen Functions --
local function place_racks(data, va, x, z, num)
	for i = 0, num - 1 do
		local x2 = x + (4 * i)
		local index = va:index(x2, 0, z)
		data[index] = c_rack
	end
end

local function place_row_signs(data, va, x, z)
	data[va:index(x, 7, z)] = c_sign
end

-- Places contents for a 16-node long row of racks
local function place_rack_contents(data, param2s, va, x, z, rotate)
	-- Rotate 180 degrees to face the right direction
	local rotation = rotate and 2 or 0

	for x2 = x, x + 15 do
		local furniture_id = bound_perlin(Perlin, #applicable_furniture, x2, 0, z)
		local c_furniture = get_content_id(facsimiles[applicable_furniture[furniture_id]])

		local loop_i = 1
		for i in va:iter(x2, 1, z, x2, 7, z) do
			if loop_i == 4 then
				data[i] = c_iw
				param2s[i] = 0
			elseif loop_i == 3 then
				data[i] = c_furniture
				param2s[i] = rotation
			else
				data[i] = c_box
				param2s[i] = rotation
			end
			loop_i = loop_i + 1
		end
	end
end

ikea_mapgen.register_department({
	name = "warehouse",
	min_depth = 20,
	max_depth = 21,

	set_data = function(data, param2s, va, x, z, edges)
		-- Place Floor --
		fill_vmanip_area(data, va, v_new(x, 0, z), v_new(x + 15, 0, z + 15), c_floor)

		-- Place Aisles --
		if should_place_aisle(x) and not (edges.e or edges.w) then
			data[va:index(x + 2, 10, z + 8)] = c_lamp
			data[va:index(x + 2, 9, z + 8)] = c_lamp_light
			data[va:index(x + 13, 10, z + 8)] = c_lamp
			data[va:index(x + 13, 9, z + 8)] = c_lamp_light

			if (edges.n and edges.inner.n) or not edges.n then
				if (edges.e and edges.inner.e) or not edges.e then
					place_row_signs(data, va, x + 15, z + 8)
					place_row_signs(data, va, x + 15, z + 9)
				end
				if (edges.w and edges.inner.w) or not edges.w then
					place_row_signs(data, va, x, z + 8)
					place_row_signs(data, va, x, z + 9)
				end
			end
			if (edges.s and edges.inner.s) or not edges.s then
				if (edges.e and edges.inner.e) or not edges.e then
					place_row_signs(data, va, x + 15, z + 0)
					place_row_signs(data, va, x + 15, z + 1)
				end
				if (edges.w and edges.inner.w) or not edges.w then
					place_row_signs(data, va, x, z + 0)
					place_row_signs(data, va, x, z + 1)
				end
			end
			-- Place Racks --
		else
			if (edges.n and edges.inner.n) or not edges.n then
				local light_index2 = va:index(x + 8, 10, z + 12)
				data[light_index2] = c_lamp
				param2s[light_index2] = 1

				local ray_index2 = va:index(x + 8, 9, z + 12)
				data[ray_index2] = c_lamp_light
				param2s[ray_index2] = 1

				place_racks(data, va, x, z + 9, 4)
				place_rack_contents(data, param2s, va, x, z + 8, false)
				place_rack_contents(data, param2s, va, x, z + 9, true)
			end
			if (edges.s and edges.inner.s) or not edges.s then
				local light_index = va:index(x + 8, 10, z + 3)
				data[light_index] = c_lamp
				param2s[light_index] = 1

				local ray_index = va:index(x + 8, 9, z + 3)
				data[ray_index] = c_lamp_light
				param2s[ray_index] = 1

				place_racks(data, va, x, z + 1, 4)
				place_rack_contents(data, param2s, va, x, z, false)
				place_rack_contents(data, param2s, va, x, z + 1, true)
			end
		end
	end,
})
