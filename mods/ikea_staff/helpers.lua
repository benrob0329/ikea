local helpers = {}

-- Local Aliases --
local m_sqrt = math.sqrt
local m_cos = math.cos
local m_sin = math.sin
local m_random = math.random
local m_max = math.max

local v_new = vector.new
local v_offset = vector.offset
local v_distance = vector.distance
local v_direction = vector.direction
local v_dot = vector.dot

local t_copy = table.copy

local get_connected_players = minetest.get_connected_players

-- Helpers --
function helpers.randompos(pos, radius)
	local t = 2 * math.pi * m_random()
	local r = m_sqrt(m_random()) * radius
	return v_new(pos.x + r * m_cos(t), pos.y, pos.z + r * m_sin(t))
end

function helpers.is_visible(pos, height, range)
	height = m_max(1, height or 0)
	for _, player in pairs(get_connected_players()) do
		local ppos = player:get_pos()
		-- Dont care about players way far away
		if v_distance(ppos, pos) <= range then
			local eye = t_copy(ppos)
			eye.y = eye.y + player:get_properties().eye_height
			local look = player:get_look_dir()
			local dir = v_direction(ppos, v_offset(pos, 0, height, 0))
			-- Only worry about players facing us
			if v_dot(look, dir) > 0 then
				-- Check all visible parts
				for i = 0, height do
					if not Raycast(eye, v_offset(pos, 0, i, 0), false):next() then
						return true
					end
				end
			end
		end
	end
	return false
end

return helpers
