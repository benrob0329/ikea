ikea_facsimile = {}
ikea_facsimile.facsimiles = {}
ikea_facsimile.actuals = {}

minetest.register_node("ikea_facsimile:container", {
	description = "Transparent node that holds an item (you hacker you!)",
	drawtype = "airlike",
	paramtype = "light",
	paramtype2 = "none",
	is_ground_content = true,
	sunlight_propagates = true,
	groups = { static = 1 },

	on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
		local name = ikea_facsimile.facsimiles[itemstack:get_name()]

		if itemstack:is_empty() or not name then
			return itemstack
		end

		node.name = name
		minetest.swap_node(pos, node)
		itemstack:clear()
		return itemstack
	end,
})

minetest.register_on_mods_loaded(function()
	for _, name in ipairs(ikea.get_nodes_by_groups({ facsimile = 1 })) do
		local def = minetest.registered_nodes[name]
		local fax_name = string.format("ikea_facsimile:%s__%s", name:match("([A-z_]+):([A-z_]+)"))
		ikea_facsimile.facsimiles[name] = fax_name
		ikea_facsimile.actuals[fax_name] = name

		minetest.register_node(":" .. fax_name, {
			description = "Facsimile node for use on storage (you hacker you!)",
			visual_scale = (12 / 16) * (def.visual_scale or 1),
			drawtype = def.drawtype,
			mesh = def.mesh,
			tiles = def.tiles,
			paramtype = "light",
			paramtype2 = def.paramtype2,
			light_source = def.light_source,
			groups = { carryable = 1 },
			drop = name,

			after_dig_node = function(pos, node, oldmetadata, digger)
				node.name = "ikea_facsimile:container"
				minetest.swap_node(pos, node)
			end,
		})
	end
end)
