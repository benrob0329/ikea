-- Local Aliases --
local v_round = vector.round

local get_player_by_name = minetest.get_player_by_name
local swap_node = minetest.swap_node
local get_node = minetest.get_node
local after = minetest.after

-- Flashlight --
minetest.register_node("ikea_tools:flashlight_light", {
	description = "Flashlight Light",
	drawtype = "airlike",
	paramtype = "light",
	light_source = minetest.LIGHT_MAX,
	buildable_to = true,
	floodable = true,
	pointable = false,
	walkable = false,
})

local function flashlight_update(name, last_light_pos)
	local player = get_player_by_name(name)
	if not player then
		if last_light_pos then
			swap_node(last_light_pos, { name = "air" })
		end
		return
	end

	local wielded_item = player:get_wielded_item()
	if wielded_item:get_name() ~= "ikea_tools:flashlight_on" then
		local inv = player:get_inventory()
		inv:set_stack("main", 1, ItemStack("ikea_tools:flashlight_off"))
		if last_light_pos then
			swap_node(last_light_pos, { name = "air" })
		end
		return
	end

	local pos = player:get_pos()
	pos.y = pos.y + (player.eye_height or 1.625)
	local look_dir = player:get_look_dir()

	local light_pos
	for pointed_thing in Raycast(pos, pos + (look_dir * 150), false, true) do
		if pointed_thing.type == "node" then
			light_pos = pointed_thing.above
			break
		end
	end

	if light_pos then
		local node_name = get_node(light_pos).name

		if node_name == "air" then
			swap_node(light_pos, { name = "ikea_tools:flashlight_light" })

			if last_light_pos then
				swap_node(last_light_pos, { name = "air" })
			end
			last_light_pos = light_pos
		elseif node_name == "ikea_tools:flashlight_light" then
			-- Do nothing, the light already exists
		else
			-- Sometimes nodeboxes and meshes can throw this off
			-- so try one block closer to the player
			light_pos = light_pos - v_round(look_dir)
			node_name = get_node(light_pos).name
			if node_name == "air" then
				swap_node(light_pos, { name = "ikea_tools:flashlight_light" })
				if last_light_pos then
					swap_node(last_light_pos, { name = "air" })
				end
				last_light_pos = light_pos
			end
		end
	else
		if last_light_pos then
			swap_node(last_light_pos, { name = "air" })
		end
		last_light_pos = false
	end

	after(0.1, flashlight_update, name, last_light_pos)
end

minetest.register_node("ikea_tools:flashlight_off", {
	description = "Flashlight (off)",
	drawtype = "mesh",
	mesh = "ikea_tools_flashlight_off.obj",
	tiles = { "ikea_tools_flashlight_off.png" },
	range = 0,

	on_secondary_use = function(itemstack, user, pointed_thing)
		itemstack:replace("ikea_tools:flashlight_on")
		after(0, flashlight_update, user:get_player_name(), false)
		return itemstack
	end,
})

minetest.register_node("ikea_tools:flashlight_on", {
	description = "Flashlight (on)",
	drawtype = "mesh",
	mesh = "ikea_tools_flashlight_on.obj",
	tiles = { "ikea_tools_flashlight_on.png" },
	range = 0,

	on_secondary_use = function(itemstack, user, pointed_thing)
		itemstack:replace("ikea_tools:flashlight_off")
		return itemstack
	end,
})
