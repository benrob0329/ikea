-- Files --
local modpath = minetest.get_modpath(minetest.get_current_modname()) .. "/"
dofile(modpath .. "nodes.lua")

-- Function Aliases --
local fill_vmanip_area = ikea_mapgen.fill_vmanip_area
local every_n_mapblocks = ikea_mapgen.every_n_mapblocks
local bound_perlin = ikea_mapgen.bound_perlin
local v_new = vector.new
local get_content_id = minetest.get_content_id

-- Settings --
local ceiling_decor_height = 7
local ceiling_decor_x_spacing = 4
local ceiling_decor_z_spacing = 2
local kitchen_wall_height = 4

local Perlin = PerlinNoise({
	offset = 0,
	scale = 1,
	spread = v_new(1, 1, 1),
	seed = 42,
	octaves = 1,
	persistence = 0,
	lacunarity = 0,
	flags = "defaults, absvalue",
})

-- Contend IDs --
local c_air = minetest.CONTENT_AIR
local c_floor = get_content_id("ikea_restaurant:floor")
local c_ceiling_decor = get_content_id("ikea_restaurant:ceiling_decor")
local c_seperator_x = get_content_id("ikea_restaurant:seperator_x")
local c_seperator_z = get_content_id("ikea_restaurant:seperator_z")
local c_table_and_chairs = get_content_id("ikea_restaurant:table_and_chairs")
local c_table_filler = get_content_id("ikea_restaurant:table_filler")
local c_kitchen_wall = get_content_id("ikea_restaurant:kitchen_wall")
local c_kitchen_food_dish = get_content_id("ikea_restaurant:kitchen_food_dish")

-- Department --
ikea_mapgen.register_department({
	name = "restaurant",
	min_depth = 21,
	max_depth = 21,

	set_data = function(data, param2s, va, x, z, edges, kd_node_min, kd_node_max)
		local kd = ikea_mapgen.kd_tree:new({
			Perlin = Perlin,
			max_depth = 2,
			min_depth = 2,
			k = 2,
			-- The mapblock position for +X and +Z will be short
			-- of the actual edge when converting to section coords
			-- so we have to add to compensate
			max_pos = { (kd_node_max[1] * 2) + 1, (kd_node_max[2] * 2) + 1 },
			min_pos = { kd_node_min[1] * 2, kd_node_min[2] * 2 },
		})

		local x_max = x + 15
		local z_max = z + 15

		-- Floor --
		fill_vmanip_area(data, va, v_new(x, 0, z), v_new(x_max, 0, z_max), c_floor)

		-- Ceiling Decor --
		for x2 = x + 4, x_max, ceiling_decor_x_spacing do
			for z2 = z, z_max, ceiling_decor_z_spacing do
				local i = va:index(x2, ceiling_decor_height, z2)
				data[i] = c_ceiling_decor
			end
		end

		-- Sections --
		for x2 = x, x_max, 8 do
			for z2 = z, z_max, 8 do
				local depth, median, min, max = kd:get_node({ x2 / 8, z2 / 8 })
				local section = bound_perlin(Perlin, 3, median[1], 0, median[2])
				min = { min[1] * 8, min[2] * 8 }
				max = { max[1] * 8, max[2] * 8 }

				local x2_max = x2 + 7
				local z2_max = z2 + 7

				local section_edges = {
					w = x2 == min[1],
					e = x2 == max[1],
					s = z2 == min[2],
					n = z2 == max[2],
				}

				if section <= 2 then
					-- Sitting Area --

					-- Seperators --
					if section_edges.w and (not edges.w or edges.inner.w) then
						fill_vmanip_area(
							data,
							va,
							v_new(x2, 1, z2 + 1),
							v_new(x2, 1, z2 + 3),
							c_seperator_z
						)
						fill_vmanip_area(
							data,
							va,
							v_new(x2, 1, z2_max - 1),
							v_new(x2, 1, z2_max - 3),
							c_seperator_z
						)
					end
					if section_edges.n and (not edges.n or edges.inner.n) then
						fill_vmanip_area(
							data,
							va,
							v_new(x2 + 1, 1, z2_max),
							v_new(x2 + 3, 1, z2_max),
							c_seperator_x
						)
						fill_vmanip_area(
							data,
							va,
							v_new(x2_max - 1, 1, z2_max),
							v_new(x2_max - 3, 1, z2_max),
							c_seperator_x
						)
					end

					-- Tables and Chairs --
					if section_edges.e then
						data[va:index(x2_max - 1, 1, z2_max - 3)] = c_table_and_chairs
						data[va:index(x2_max - 2, 1, z2_max - 3)] = c_table_filler
						data[va:index(x2_max - 1, 1, z2_max - 4)] = c_table_filler
						param2s[va:index(x2_max - 1, 1, z2_max - 4)] = 2
						data[va:index(x2_max - 2, 1, z2_max - 4)] = c_table_filler
						param2s[va:index(x2_max - 2, 1, z2_max - 4)] = 2

						data[va:index(x2_max - 3, 1, z2_max - 3)] = c_seperator_z
						data[va:index(x2_max - 3, 1, z2_max - 4)] = c_seperator_z

						data[va:index(x2_max - 4, 1, z2_max - 3)] = c_table_and_chairs
						data[va:index(x2_max - 5, 1, z2_max - 3)] = c_table_filler
						data[va:index(x2_max - 4, 1, z2_max - 4)] = c_table_filler
						param2s[va:index(x2_max - 4, 1, z2_max - 4)] = 2
						data[va:index(x2_max - 5, 1, z2_max - 4)] = c_table_filler
						param2s[va:index(x2_max - 5, 1, z2_max - 4)] = 2
					elseif section_edges.w then
						data[va:index(x2 + 2, 1, z2_max - 3)] = c_table_and_chairs
						data[va:index(x2 + 1, 1, z2_max - 3)] = c_table_filler
						data[va:index(x2 + 1, 1, z2_max - 4)] = c_table_filler
						param2s[va:index(x2 + 1, 1, z2_max - 4)] = 2
						data[va:index(x2 + 2, 1, z2_max - 4)] = c_table_filler
						param2s[va:index(x2 + 2, 1, z2_max - 4)] = 2

						data[va:index(x2 + 3, 1, z2_max - 3)] = c_seperator_z
						data[va:index(x2 + 3, 1, z2_max - 4)] = c_seperator_z

						data[va:index(x2 + 5, 1, z2_max - 3)] = c_table_and_chairs
						data[va:index(x2 + 4, 1, z2_max - 3)] = c_table_filler
						data[va:index(x2 + 4, 1, z2_max - 4)] = c_table_filler
						param2s[va:index(x2 + 4, 1, z2_max - 4)] = 2
						data[va:index(x2 + 5, 1, z2_max - 4)] = c_table_filler
						param2s[va:index(x2 + 5, 1, z2_max - 4)] = 2

					end
				elseif section <= 3 then
					-- Kitchen --

					local offset_w, offest_e
					if edges.w then
						offset_w = 0
						offset_e = -4
					elseif edges.e then
						offset_w = 4
						offset_e = 0
					else
						offset_w = 0
						offset_e = 0
					end

					if section_edges.w then
						-- Wall --
						fill_vmanip_area(
							data,
							va,
							v_new(x2 + offset_w, 1, z2),
							v_new(x2 + offset_w, kitchen_wall_height, z2_max),
							c_kitchen_wall
						)

						-- Food Dishes --
						if not edges.w or edges.inner.w then
							fill_vmanip_area(
								data,
								va,
								v_new(x2 + offset_w, 3, z2 + 1),
								v_new(x2 + offset_w, 3, z2_max - 1),
								c_air
							)
							fill_vmanip_area(
								data,
								va,
								v_new(x2 + offset_w, 2, z2 + 1),
								v_new(x2 + offset_w, 2, z2_max - 1),
								c_kitchen_food_dish
							)
						end
					end
					if section_edges.e then
						-- Wall --
						fill_vmanip_area(
							data,
							va,
							v_new(x2_max + offset_e, 1, z2),
							v_new(x2_max + offset_e, kitchen_wall_height, z2_max),
							c_kitchen_wall
						)

						-- Food Dishes --
						if not edges.e or edges.inner.e then
							fill_vmanip_area(
								data,
								va,
								v_new(x2_max + offset_e, 3, z2 + 1),
								v_new(x2_max + offset_e, 3, z2_max - 1),
								c_air
							)
							fill_vmanip_area(
								data,
								va,
								v_new(x2_max + offset_e, 2, z2 + 1),
								v_new(x2_max + offset_e, 2, z2_max - 1),
								c_kitchen_food_dish
							)
						end
					end
					if section_edges.s then
						-- Wall --
						if section_edges.e then
							fill_vmanip_area(
								data,
								va,
								v_new(x2, 1, z2),
								v_new(x2_max + offset_e, kitchen_wall_height, z2),
								c_kitchen_wall
							)
						elseif section_edges.w then
							fill_vmanip_area(
								data,
								va,
								v_new(x2 + offset_w, 1, z2),
								v_new(x2_max, kitchen_wall_height, z2),
								c_kitchen_wall
							)
						else
							fill_vmanip_area(
								data,
								va,
								v_new(x2, 1, z2),
								v_new(x2_max, kitchen_wall_height, z2),
								c_kitchen_wall
							)
						end
					end
					if section_edges.n then
						-- Wall --
						if section_edges.e then
							fill_vmanip_area(
								data,
								va,
								v_new(x2, 1, z2_max),
								v_new(x2_max + offset_e, kitchen_wall_height, z2_max),
								c_kitchen_wall
							)
						elseif section_edges.w then
							fill_vmanip_area(
								data,
								va,
								v_new(x2 + offset_w, 1, z2_max),
								v_new(x2_max, kitchen_wall_height, z2_max),
								c_kitchen_wall
							)
						else
							fill_vmanip_area(
								data,
								va,
								v_new(x2, 1, z2_max),
								v_new(x2_max, kitchen_wall_height, z2_max),
								c_kitchen_wall
							)
						end
					end
				end
			end
		end
	end,
})
