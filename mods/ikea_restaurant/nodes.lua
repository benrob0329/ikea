minetest.register_node("ikea_restaurant:floor", {
	description = "Restaurant Floor (you hacker you!)",
	drawtype = "normal",
	tiles = {
		{ name = "ikea_restaurant_floor.png", align_style = "world", scale = 16 },
	},
	paramtype = "light",
	sunlight_propagates = true,
	groups = { static = 1 },
})

minetest.register_node("ikea_restaurant:ceiling_decor", {
	description = "Restaurant Ceiling Decor (you hacker you!)",
	drawtype = "mesh",
	mesh = "ikea_restaurant_ceiling_decor.obj",
	tiles = {
		{ name = "ikea_restaurant_ceiling_decor.png", backface_culled = false },
	},
	paramtype = "light",
	sunlight_propagates = true,
	groups = { static = 1 },
})

minetest.register_node("ikea_restaurant:seperator_x", {
	description = "Restaurant Seperation Wall (X Axis) (you hacker you!)",
	drawtype = "nodebox",
	node_box = {
		type = "connected",
		fixed = { -8 / 16, -8 / 16, -4 / 16, 8 / 16, 8 / 16, 4 / 16 },
		disconnected_left = { -8 / 16, -8 / 16, -4 / 16, -12 / 16, 8 / 16, 4 / 16 },
		disconnected_right = { 8 / 16, -8 / 16, -4 / 16, 12 / 16, 8 / 16, 4 / 16 },
	},
	connect_sides = { "left", "right" },
	connects_to = { "ikea_restaurant:seperator_x" },
	tiles = {
		{
			name = "ikea_restaurant_seperator_top.png",
			align_style = "world",
			scale = 1,
		},
		{
			name = "ikea_restaurant_seperator_top.png",
			align_style = "world",
			scale = 1,
		},
		{ name = "ikea_restaurant_seperator_side.png" },
		{ name = "ikea_restaurant_seperator_side.png" },
		{
			name = "ikea_restaurant_seperator_front.png",
			align_style = "world",
			scale = 1,
		},
		{
			name = "ikea_restaurant_seperator_front.png",
			align_style = "world",
			scale = 1,
		},
	},
	paramtype = "light",
	sunlight_propagates = true,
	groups = { static = 1 },
})

minetest.register_node("ikea_restaurant:seperator_z", {
	description = "Restaurant Seperation Wall (Z Axis) (you hacker you!)",
	drawtype = "nodebox",
	node_box = {
		type = "connected",
		fixed = { -4 / 16, -8 / 16, -8 / 16, 4 / 16, 8 / 16, 8 / 16 },
		disconnected_front = { -4 / 16, -8 / 16, -8 / 16, 4 / 16, 8 / 16, -12 / 16 },
		disconnected_back = { -4 / 16, -8 / 16, 8 / 16, 4 / 16, 8 / 16, 12 / 16 },
	},
	connect_sides = { "front", "back" },
	connects_to = { "ikea_restaurant:seperator_z" },
	tiles = {
		{
			name = "ikea_restaurant_seperator_top.png^[transformR90",
			align_style = "world",
			scale = 1,
		},
		{
			name = "ikea_restaurant_seperator_top.png^[transformR90",
			align_style = "world",
			scale = 1,
		},
		{
			name = "ikea_restaurant_seperator_front.png",
			align_style = "world",
			scale = 1,
		},
		{
			name = "ikea_restaurant_seperator_front.png",
			align_style = "world",
			scale = 1,
		},
		{ name = "ikea_restaurant_seperator_side.png" },
		{ name = "ikea_restaurant_seperator_side.png" },
	},
	paramtype = "light",
	sunlight_propagates = true,
	groups = { static = 1 },
})

minetest.register_node("ikea_restaurant:table_and_chairs", {
	description = "Restaurant Table and Chairs (you hacker you!)",
	drawtype = "mesh",
	mesh = "ikea_restaurant_table_and_chairs.obj",
	tiles = { { name = "ikea_restaurant_kitchen_table_and_chairs.png" } },
	paramtype = "light",
	groups = { static = 1, ikea_food_table = 1 },
})

minetest.register_node("ikea_restaurant:table_filler", {
	description = "Restaurant Table and Chairs Filler (you hacker you!)",
	drawtype = "airlike",
	pointable = true,
	walkable = true,
	paramtype = "light",
	paramtype2 = "facedir",
	groups = { static = 1, ikea_food_table = 1 },
})

minetest.register_node("ikea_restaurant:kitchen_wall", {
	description = "Restaurant Table and Chairs (you hacker you!)",
	drawtype = "normal",
	tiles = { { name = "ikea_restaurant_kitchen_wall.png" } },
	paramtype = "light",
	groups = { static = 1 },
})

minetest.register_node(
	"ikea_restaurant:kitchen_food_dish",
	table.merge(ikea.default_mesh(), {
		description = "Restaurant Food Dish (you hacker you!)",
		mesh = "ikea_restaurant_kitchen_food_dish.obj",
		tiles = { { name = "ikea_restaurant_kitchen_food_dish.png" } },
		groups = { static = 1 },

		on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
			if itemstack:is_empty() then
				return ItemStack("ikea_restaurant:plate_of_food")
			else
				return itemstack
			end
		end,
	})
)

minetest.register_node("ikea_restaurant:plate", table.merge(
	ikea.default_mesh(),
	{
		description = "Restaurant Plate (Empty)",
		mesh = "ikea_restaurant_plate.obj",
		tiles = {{name = "ikea_restaurant_plate.png"}},
		groups = {carryable = 1},
	}
))

minetest.register_entity("ikea_restaurant:food", {
	initial_properties = {
		visual = "item",
		visual_size = vector.new(0.5, 0.5, 0.5),
		physical = false,
		collide_with_objects = false,
		pointable = false,
	},
	on_activate = function(self, staticdata, dtime_s)
		local props = self.object:get_properties()
		props.wield_item = staticdata
		self.object:set_properties(props)
	end,
	on_step = function(self, dtime, moveresult)
		self._timer = (self._timer or 0) + dtime
		if self._timer < 10 then
			return
		else
			self.object:remove()
		end
	end,
})

minetest.register_node("ikea_restaurant:plate_of_food", table.merge(
	ikea.default_mesh(),
	{
		description = "Restaurant Plate (Full)",
		mesh = "ikea_restaurant_plate.obj",
		tiles = {{name = "ikea_restaurant_plate_of_food.png"}},
		groups = {carryable = 1},

		on_place = function(itemstack, placer, pointed_thing)
			local node = minetest.get_node(pointed_thing.under)

			if node and (minetest.get_item_group(node.name, "ikea_food_table") == 1) then
				local seat_pos = (minetest.facedir_to_dir(node.param2) * 0.75) + pointed_thing.under
				local player_pos = placer:get_pos()
				local meta = placer:get_meta()
				local hunger = meta:get_int("ikea_hunger")
			
				placer:set_physics_override({speed = 0})
				placer:set_pos(seat_pos)

				minetest.add_entity(vector.offset(pointed_thing.under, 0, 1, 0), "ikea_restaurant:food", "ikea_restaurant:plate_of_food")
			
				local delay = 0
				for i = hunger, 0, -1 do
					delay = delay + 1
					minetest.after(delay, meta.set_int, meta, "ikea_hunger", i)
				end
			
				minetest.after(10, function()
					placer:set_pos(player_pos)
					placer:set_physics_override({speed = 1})
				end)

				return ItemStack("")
			else
				return nil
			end
		end,
	}
))
