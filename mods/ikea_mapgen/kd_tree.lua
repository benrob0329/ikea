-- Function Aliases --
local bound_perlin = ikea_mapgen.bound_perlin
local tail_perlin = ikea_mapgen.tail_perlin
local m_floor = math.floor
local m_ceil = math.ceil
local m_round = math.round
local m_abs = math.abs

-- K-D Tree Object --
local kd_tree = {}

function kd_tree:new(o)
	assert(o, "Must pass options to kd_tree:new!")
	assert(o.Perlin, "Must pass Perlin noise object to kd_tree:new!")
	assert(type(o.max_depth) == "number", "Must pass a valid max_depth to kd_tree:new!")
	assert(type(o.min_depth) == "number", "Must pass a valid min_depth to kd_tree:new!")
	assert(type(o.k) == "number", "Must pass a valid k to kd_tree:new!")
	assert(type(o.min_pos) == "table", "Must pass a valid min_pos to kd_tree:new!")
	assert(type(o.max_pos) == "table", "Must pass a valid max_pos to kd_tree:new!")

	setmetatable(o, self)
	self.__index = self

	return o
end

function kd_tree:get_node(pos)
	local k, Perlin = self.k, self.Perlin
	local min, max = { unpack(self.min_pos) }, { unpack(self.max_pos) }
	local min_depth, max_depth = self.min_depth, self.max_depth
	local depth = 0

	while true do
		local median, half_width = {}, {}
		for i = 1, k do
			half_width[i] = m_abs(max[i] - min[i]) / 2
			median[i] = min[i] + half_width[i]
		end

		local should_split = false

		if depth < min_depth then
			should_split = true
		elseif depth < max_depth then
			should_split = bound_perlin(Perlin, 2, median[1], median[2] or depth, median[3] or 0)
				== 1
		end

		if not should_split then
			return depth, median, min, max
		end

		local axis = (depth % k) + 1

		if pos[axis] < median[axis] then
			-- closer to child node A (left side)
			max[axis] = max[axis] - m_ceil(half_width[axis] + 0.5)
		else
			-- closer to child node B (right side)
			min[axis] = min[axis] + m_floor(half_width[axis] + 0.5)
		end

		depth = depth + 1
	end
end

return kd_tree
