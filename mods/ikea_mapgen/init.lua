-- Function Aliases --
local get_content_id = minetest.get_content_id
local t_insert = table.insert
local t_merge = table.merge
local m_round = math.round
local m_floor = math.floor
local m_abs = math.abs
local v_new = vector.new
local v_sort = vector.sort

-- Nodes --
minetest.register_node("ikea_mapgen:ceiling", {
	description = "Ceiling node (you hacker you!)",
	paramtype = "light",
	tiles = { { name = "ikea_ceiling.png", scale = 16, align_style = "world" } },
	groups = { static = 1 },
	sunlight_propagates = true,
})

minetest.register_node("ikea_mapgen:skylight", {
	description = "Skylight node (you hacker you!)",
	drawtype = "glasslike_framed",
	paramtype = "light",
	paramtype2 = "glasslikeliquidlevel",
	tiles = { "ikea_skylight.png", "ikea_skylight_detail.png" },
	groups = { static = 1, ikea_fx_dust = 1 },
	sunlight_propagates = true,
	light_source = 2,
	pointable = false,
	ikea_fx_dust_amount = 1,
	ikea_fx_dust_spawner_chance = 32,

})

minetest.register_node("ikea_mapgen:spawn_blacktop", {
	description = "Blacktop node for spawn (you hacker you!)",
	tiles = {{name = "ikea_spawn_blacktop.png", scale = 16, align_style = "world" }},
	paramtype = "light",
	groups = {static = 1},
	sunlight_propagates = true,
})

minetest.register_node("ikea_mapgen:spawn_parking", {
	description = "Blacktop node for the parking lot (you hacker you!)",
	tiles = {{name = "ikea_spawn_parking.png", scale = 16, align_style = "world" }},
	paramtype = "light",
	groups = {static = 1},
	sunlight_propagates = true,
})

minetest.register_node("ikea_mapgen:spawn_blue", {
	description = "Blue node for the spawn building (you hacker you!)",
	tiles = {{name = "ikea_spawn_blue.png", scale = 16, align_style = "world" }},
	paramtype = "light",
	groups = {static = 1},
	sunlight_propagates = true,
})

minetest.register_node("ikea_mapgen:spawn_yellow", {
	description = "Yellow node for the spawn building (you hacker you!)",
	tiles = {{name = "ikea_spawn_yellow.png", scale = 16, align_style = "world" }},
	paramtype = "light",
	groups = {static = 1},
	sunlight_propagates = true,
})

-- Registration Function --
local departments = {}
local department_defaults = {
	name = "rename me",
	set_data = function(data, param2s, va, x, z, edges)
		data[1] = get_content_id("ikea:error")
	end,
	on_place = function(context, vm, x, z) end,
}

local function register_department(def_raw)
	def_raw = def_raw or {}

	-- Use new table to avoid changing the defaults table
	local def = {}
	t_merge(def, department_defaults)
	t_merge(def, def_raw)

	t_insert(departments, def)
end

-- Utility Functions --
local function node_or_ignore(node)
	if type(node) == "string" then
		return { name = node }
	elseif type(node) == "table" then
		return node
	else
		return { name = "ignore" }
	end
end

local function every_n_mapblocks(n)
	return function(value)
		return (value / 16) % n == 0
	end
end

local function fill_vmanip_area(data, va, p1, p2, c_id)
	p1, p2 = v_sort(p1, p2)
	for i in va:iterp(p1, p2) do
		data[i] = c_id
	end
end

-- Returns The Decimal Places Of A Random Number
local function tail_perlin(Perlin, x, y, z)
	local num = Perlin:get_3d({ x = x, y = y, z = z })
	return num - m_floor(num)
end

-- Returns A Random Number From 1 to limit
local function bound_perlin(Perlin, limit, x, y, z)
	local num = tail_perlin(Perlin, x, y, z)
	return m_round(num * (limit - 1)) + 1
end

-- Global Namespace --
ikea_mapgen = {
	register_department = register_department,
	node_or_ignore = node_or_ignore,
	every_n_mapblocks = every_n_mapblocks,
	fill_vmanip_area = fill_vmanip_area,
	tail_perlin = tail_perlin,
	bound_perlin = bound_perlin,
}

-- This needs the global namespace before it can be run
local kd_tree = dofile(minetest.get_modpath(minetest.get_current_modname()) .. "/kd_tree.lua")
ikea_mapgen.kd_tree = kd_tree

do -- Map Generation --
	-- Wait until mapgen initializes to get the K-D Tree and Perlin
	local kd, Perlin
	minetest.after(0, function()
		-- Limits --
		local mapgen_block_limit = m_round(minetest.settings:get("mapgen_limit") / 16)

		local min_depth = 100
		local max_depth = 0
		for _, department in ipairs(departments) do
			if department.min_depth < min_depth then
				min_depth = department.min_depth
			end
			if department.max_depth > max_depth then
				max_depth = department.max_depth
			end
		end

		Perlin = minetest.get_perlin({
			offset = 0,
			scale = 1,
			spread = { x = 1, y = 1, z = 1 },
			seed = 42,
			octaves = 1,
			persistence = 0,
		})

		kd = kd_tree:new({
			Perlin = Perlin,
			max_depth = max_depth,
			min_depth = min_depth,
			k = 2,
			min_pos = { -mapgen_block_limit, -mapgen_block_limit },
			max_pos = { mapgen_block_limit, mapgen_block_limit },
		})
	end)

	-- Settings --
	local ceiling_height = 15

	-- Content IDs --
	local c_ceiling = get_content_id("ikea_mapgen:ceiling")
	local c_skylight = get_content_id("ikea_mapgen:skylight")
	local c_parking = get_content_id("ikea_mapgen:spawn_parking")
	local c_blacktop = get_content_id("ikea_mapgen:spawn_blacktop")
	local c_blue = get_content_id("ikea_mapgen:spawn_blue")
	local c_yellow = get_content_id("ikea_mapgen:spawn_yellow")

	-- Local Functions --
	function get_department(depth, median)
		local applicable_departments = {}

		for _, v in ipairs(departments) do
			if (depth <= v.max_depth) and (depth >= v.min_depth) then
				t_insert(applicable_departments, v)
			end
		end

		return applicable_departments[bound_perlin(
			Perlin,
			#applicable_departments,
			median[1],
			42,
			median[2]
		) or 1]
	end

	minetest.register_on_generated(function(minp, maxp, seed)
		local vm, emin, emax = minetest.get_mapgen_object("voxelmanip")
		local data = vm:get_data()
		local param2s = vm:get_param2_data()
		local va = VoxelArea:new({ MinEdge = emin, MaxEdge = emax })

		for z = minp.z, maxp.z, 16 do
			for x = minp.x, maxp.x, 16 do
				if va:contains(x, 0, z) then
					local mapblock_x = x / 16
					local mapblock_z = z / 16

					local node_depth, node_median, node_min, node_max = kd:get_node({
						mapblock_x,
						mapblock_z,
					})

					local department = get_department(node_depth, node_median)

					local edges = {
						w = mapblock_x == node_min[1],
						e = mapblock_x == node_max[1],
						s = mapblock_z == node_min[2],
						n = mapblock_z == node_max[2],
						inner = {},
					}

					for k, v in pairs({
						n = { mapblock_x, mapblock_z + 1 },
						s = { mapblock_x, mapblock_z - 1 },
						e = { mapblock_x + 1, mapblock_z },
						w = { mapblock_x - 1, mapblock_z },
					}) do
						if edges[k] then
							local neighbor_depth, neighbor_median, _, _ = kd:get_node(v)
							local adjacent_dept = get_department(neighbor_depth, neighbor_median)
							edges.inner[k] = adjacent_dept.name == department.name
						end
					end

					department.set_data(data, param2s, va, x, z, edges, node_min, node_max)

					fill_vmanip_area(
						data,
						va,
						v_new(x, ceiling_height, z),
						v_new(x + 15, ceiling_height, z + 15),
						c_ceiling
					)

					if bound_perlin(Perlin, 8, mapblock_x, 42, mapblock_z) == 1 then
						fill_vmanip_area(
							data,
							va,
							v_new(x + 4, ceiling_height, z + 1),
							v_new(x + 15 - 4, ceiling_height, z + 15 - 1),
							c_skylight
						)
					end
				elseif va:contains(x, 10000, z) then
					-- Add parking/blacktop --
					if z < 0 then
						fill_vmanip_area(data, va, v_new(x, 10000, z), v_new(x + 15, 10000, z + 15), c_parking)
					else
						fill_vmanip_area(data, va, v_new(x, 10000, z), v_new(x + 15, 10000, z + 15), c_blacktop)
					end
				end
			end
		end

		-- Spawn Building --
		if va:contains(0, 10000, 100) then
			fill_vmanip_area(data, va, v_new(-25, 10001, 50), v_new(25, 10025, 100), c_blue)
			fill_vmanip_area(data, va, v_new(-25, 10001, 10), v_new(-50, 10015, 100), c_yellow)
		end

		vm:set_data(data)
		vm:set_param2_data(param2s)
		minetest.generate_decorations(vm, minp, maxp)
		vm:update_liquids()
		vm:calc_lighting()
		vm:write_to_map()
	end)
end
