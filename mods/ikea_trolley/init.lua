-- Local Aliases --
local t_merge = table.merge
local v_new = vector.new
local v_offset = vector.offset

local facsimiles = ikea_facsimile.facsimiles

local get_node = minetest.get_node
local set_node = minetest.set_node
local remove_node = minetest.remove_node
local add_entity = minetest.add_entity
local dir_to_facedir = minetest.dir_to_facedir
local item_place = minetest.item_place
local get_player_by_name = minetest.get_player_by_name

-- Helpers --
local item_positions = {
	[0] = {
		vector.new(0, 0, 1),
		vector.new(1, 0, 1),
		vector.new(0, 0, 2),
		vector.new(1, 0, 2),
		vector.new(0, 0, 3),
		vector.new(1, 0, 3),
	},
	[1] = {
		vector.new(1, 0, -1),
		vector.new(1, 0, 0),
		vector.new(2, 0, -1),
		vector.new(2, 0, 0),
		vector.new(3, 0, -1),
		vector.new(3, 0, 0),
	},
	[2] = {
		vector.new(-1, 0, -1),
		vector.new(0, 0, -1),
		vector.new(-1, 0, -2),
		vector.new(0, 0, -2),
		vector.new(-1, 0, -3),
		vector.new(0, 0, -3),
	},
	[3] = {
		vector.new(-1, 0, 0),
		vector.new(-1, 0, 1),
		vector.new(-2, 0, 0),
		vector.new(-2, 0, 1),
		vector.new(-3, 0, 0),
		vector.new(-3, 0, 1),
	},
}

local function do_on_trolley_container_positions(pos, param2, func)
	for i, vec in ipairs(item_positions[param2 % 4]) do
		local c_pos = vec + pos
		func(c_pos, i)
	end
end

-- Node Def --
local trolley_box = {
	type = "fixed",
	fixed = {
		{ -0.5, -0.250543, -0.5, 1.5, -0.500882, 3.5 },
		{ -0.5, 1.25088, -0.5, -0.375, -0.250543, 0.25 },
		{ 1.375, 1.25088, -0.5, 1.5, -0.250543, 0.25 },
	},
}

minetest.register_node(
	"ikea_trolley:flatbed",
	t_merge(ikea.default_mesh(), {
		description = "Flatbed Trolley",
		mesh = "ikea_trolley_flatbed_node.obj",
		tiles = { "ikea_trolley_flatbed.png" },
		selection_box = trolley_box,
		collision_box = trolley_box,
		wield_scale = vector.new(0, 0, 0),
		node_placement_prediction = "",
		groups = { carryable = 1 },

		preserve_metadata = function(pos, oldnode, oldmeta, drops)
			local meta = drops[1]:get_meta()
			do_on_trolley_container_positions(pos, oldnode.param2, function(c_pos, i)
				local item = minetest.get_node(c_pos)
				local item_name = ikea_facsimile.actuals[item.name]
				if not item_name then
					return
				end

				meta:set_string(i, item_name)
			end)
		end,
		after_dig_node = function(pos, oldnode, oldmetadata, digger)
			local obj = minetest.add_entity(pos, "ikea_trolley:flatbed", digger:get_player_name())
			if not obj then
				return
			end
			obj:set_attach(digger, "", v_new(0, 5, 10), v_new(0, 0, 0), true)

			do_on_trolley_container_positions(pos, oldnode.param2, function(c_pos, i)
				local item = get_node(c_pos)
				remove_node(c_pos)
				local item_name = ikea_facsimile.actuals[item.name]
				if not item_name then
					return
				end

				local item_obj = add_entity(pos, "ikea_trolley:item", item_name)
				if item_obj then
					item_obj:set_attach(
						obj,
						"",
						v_offset(item_positions[0][i] * 10, -5, 0, 0),
						v_new(0, 180, 0),
						true
					)
				end
			end)
		end,
		on_place = function(itemstack, placer, pointed_thing)
			if not placer then
				return
			end
			local pos = pointed_thing.above
			local param2 = dir_to_facedir(placer:get_look_dir())

			local can_place = true
			do_on_trolley_container_positions(pos, param2, function(c_pos, i)
				if get_node(c_pos).name ~= "air" then
					can_place = false
				end
			end)

			if can_place then
				return item_place(itemstack, placer, pointed_thing)
			end
		end,
		after_place_node = function(pos, placer, itemstack, pointed_thing)
			local meta = itemstack:get_meta()
			local trolley_node = get_node(pos)
			do_on_trolley_container_positions(pos, trolley_node.param2, function(c_pos, i)
				local name = facsimiles[meta:get_string(i)] or "ikea_facsimile:container"
				local param2 = (trolley_node.param2 + 2) % 4 -- Flip the rotation by 180°

				set_node(c_pos, { name = name, param2 = param2 })
			end)
		end,
	})
)

minetest.register_entity("ikea_trolley:item", {
	initial_properties = {
		visual = "item",
		visual_size = v_new(0.5, 0.5, 0.5),
		physical = false,
		collide_with_objects = false,
		pointable = false,
	},
	on_activate = function(self, staticdata, dtime_s)
		local props = self.object:get_properties()
		props.wield_item = staticdata
		self.object:set_properties(props)
	end,
	on_step = function(self, dtime, moveresult)
		self._timer = (self._timer or 0) + dtime
		if self._timer < 0.1 then
			return
		else
			self._timer = 0
		end

		if not self.object:get_attach() then
			self.object:remove()
		end
	end,
})

-- Visual-Only Entity --
minetest.register_entity("ikea_trolley:flatbed", {
	initial_properties = {
		visual = "mesh",
		mesh = "ikea_trolley_flatbed_entity.obj",
		textures = { "ikea_trolley_flatbed.png" },
		physical = false,
		collide_with_objects = false,
		pointable = false,
	},
	on_activate = function(self, staticdata, dtime_s)
		self._playername = staticdata
	end,
	on_step = function(self, dtime, moveresult)
		self._timer = (self._timer or 0) + dtime
		if self._timer < 0.2 then
			return
		else
			self._timer = 0
		end

		local player = get_player_by_name(self._playername)
		if not player then
			self.object:remove()
			return
		end

		local inv = player:get_inventory()
		if not inv:contains_item("main", ItemStack("ikea_trolley:flatbed"), false) then
			self.object:remove()
			return
		end
	end,
})
