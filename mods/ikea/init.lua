DEBUG = minetest.settings:get_bool("DEBUG") or false

do -- Misc Utility Stuff --
	ikea = {}
	ikea.opening_time = 7
	ikea.closing_time = 23

	function ikea.is_open()
		local tod = minetest.get_timeofday()
		return tod >= (ikea.opening_time / 24) and tod <= (ikea.closing_time / 24)
	end

	local registered_on_tod = {}
	function ikea.register_on_tod(tod, func)
		if (type(tod) == "number") and (type(func) == "function") then
			registered_on_tod[tod] = registered_on_tod[tod] or {}
			table.insert(registered_on_tod[tod], func)
		end
	end

	local old_tod = 0
	local timer = 0
	minetest.register_globalstep(function(dtime)
		timer = timer + dtime
		if timer >= 1 then
			local tod = math.floor(minetest.get_timeofday() * 24)
			if (tod ~= old_tod) and registered_on_tod[tod] then
				for _, v in ipairs(registered_on_tod[tod]) do
					v()
				end
				old_tod = tod
			end
			timer = 0
		end
	end)

	-- Returns A List Of Nodestrings Filtered By Group
	function ikea.get_nodes_by_groups(groups)
		groups = groups or {}
		local results = {}

		for node_name, _ in pairs(minetest.registered_nodes) do
			local should_include = true
			for group, value in pairs(groups) do
				if minetest.get_item_group(node_name, group) ~= value then
					should_include = false
				end
			end
			if should_include then
				table.insert(results, node_name)
			end
		end

		return results
	end

	function ikea.debug(...)
		print(dump({ ... }))
		return ...
	end
end

do -- Nodes --
	minetest.register_node("ikea:error", {
		drawtype = "mesh",
		mesh = "error.obj",
		tiles = { "unknown_node.png^[colorize:#ff0000:255" },
		pointable = false,
		walkable = false,
	})

	minetest.register_node("ikea:invisible_wall", {
		description = "Invisible Node For Collisions (You Hacker!)",
		paramtype = "light",
		drawtype = "airlike",
		walkable = true,
		pointable = false,
		is_ground_content = true,
		sunlight_propagates = true,
	})
end

-- Default Nodedef Tables --
local error_node = minetest.registered_nodes["ikea:error"]

function ikea.default_mesh()
	return {
		drawtype = "mesh",
		mesh = error_node.mesh,
		tiles = error_node.tiles,
		paramtype = "light",
		paramtype2 = "facedir",
	}
end
