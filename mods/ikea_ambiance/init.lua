-- Power Down Sound --
ikea.register_on_tod(ikea.closing_time, function()
	minetest.sound_play({ name = "ikea_power_down", gain = 0.15, pitch = 1.0 })
end)

do -- Music --
	local tracks = {
		{
			name = "jahzzar_cddc_aisles",
			length = 216,
			title = "Aisles",
			author = "Jahzzar",
			license = "CC-BY-SA",
		},
		{
			name = "jahzzar_cddc_deadpanned",
			length = 274,
			title = "Deadpanned",
			author = "Jahzzar",
			license = "CC-BY-SA",
		},
		{
			name = "jahzzar_cddc_mantra",
			length = 176,
			title = "Mantra",
			author = "Jahzzar",
			license = "CC-BY-SA",
		},
		{
			name = "jahzzar_cddc_pure",
			length = 246,
			title = "Pure",
			author = "Jahzzar",
			license = "CC-BY-SA",
		},
		{
			name = "jahzzar_cddc_siste_viator",
			length = 118,
			title = "Siste Viator",
			author = "Jahzzar",
			license = "CC-BY-SA",
		},
	}

	local music_gain = 0.1
	local last_played_id = 0
	local handles = {}

	local function display_song_info(song_id)
		local track = tracks[song_id]
		local message = string.format("%s by %s, %s", track.title, track.author, track.license)
		minetest.chat_send_all(message)
	end

	local function stop()
		for _, player in ipairs(minetest.get_connected_players()) do
			local handle = handles[player:get_player_name()]
			if handle then
				minetest.sound_stop(handle)
			end
		end
	end

	-- Play the song to each player, store the handle in meta
	local function play_to_all(song_id)
		for _, player in ipairs(minetest.get_connected_players()) do
			local song_name = tracks[song_id].name
			local player_name = player:get_player_name()

			handles[player_name] = minetest.sound_play({
				name = song_name,
				to_player = player_name,
				gain = music_gain * (player:get_meta():get_float("music:volume")),
			})
		end
	end

	-- Play a different, random track
	local function play_random_to_all()
		local song_id
		repeat
			song_id = math.random(1, #tracks)
		until song_id ~= last_played_id

		play_to_all(song_id)
		display_song_info(song_id)

		last_played_id = song_id
	end

	-- Set default music volume
	minetest.register_on_newplayer(function(player)
		player:get_meta():set_float("music:volume", 1)
	end)

	-- Delete Handle When Player Leaves
	minetest.register_on_leaveplayer(function(player, timed_out)
		handles[player:get_player_name()] = nil
	end)

	minetest.register_chatcommand("music_volume", {
		description = "Set music volume.",
		params = "<float >= 0>",
		func = function(name, param)
			local player = minetest.get_player_by_name(name)
			if not player then
				return false, "You're not a player??"
			end

			local meta = player:get_meta()
			if not meta then
				return false, "Failed to get meta data!"
			end

			param = tonumber(param)

			if tonumber(param) and param >= 0 then
				meta:set_float("music:volume", param)
				minetest.sound_fade(handles[name], 1, music_gain * param)

				return true, ("Music volume set to %.1f."):format(param)
			elseif not param then
				return true, ("Music volume is set to %.1f."):format(meta:get_float("music:volume"))
			else
				return false, "Invalid usage, see /help volume."
			end
		end,
	})

	ikea.register_on_tod(ikea.opening_time, function()
		stop()
		play_random_to_all()
	end)

	ikea.register_on_tod(12, play_random_to_all)
	ikea.register_on_tod(ikea.closing_time - 2, play_random_to_all)

	ikea.register_on_tod(ikea.closing_time, function()
		stop()
	end)
end
