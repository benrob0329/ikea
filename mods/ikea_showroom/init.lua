-- Files --
local modpath = minetest.get_modpath("ikea_showroom")
dofile(modpath .. "/nodes.lua")
local rooms, displays = dofile(modpath .. "/displays.lua")

-- Local aliases --
local fill_vmanip_area = ikea_mapgen.fill_vmanip_area
local bound_perlin = ikea_mapgen.bound_perlin
local v_new = vector.new

local CONTENT_AIR = minetest.CONTENT_AIR

-- Settings --
local place_opening = ikea_mapgen.every_n_mapblocks(4)
local wall_height = 10
local Perlin = PerlinNoise({
	offset = 0,
	scale = 15,
	spread = v_new(10, 10, 10),
	seed = 2,
	octaves = 2,
	persistence = 3,
	lacunarity = 2,
	flags = "defaults, absvalue",
})

-- Content IDs --
local c_wall = minetest.get_content_id("ikea_showroom:wall")
local c_floor = minetest.get_content_id("ikea_showroom:floor")
local c_trolley = minetest.get_content_id("ikea_trolley:flatbed")
local c_container = minetest.get_content_id("ikea_facsimile:container")

ikea_mapgen.register_department({
	name = "showroom",
	min_depth = 20,
	max_depth = 21,

	set_data = function(data, param2s, va, x, z, edges)
		-- Corner North/South East/West Top/Bottom
		local corner_swb = v_new(x + 0, 0, z + 0)
		local corner_seb = v_new(x + 15, 0, z + 0)
		local corner_nwb = v_new(x + 0, 0, z + 15)

		local corner_set = v_new(x + 15, wall_height, z + 0)
		local corner_nwt = v_new(x + 0, wall_height, z + 15)
		local corner_net = v_new(x + 15, wall_height, z + 15)

		-- Walls --
		if edges.s then
			fill_vmanip_area(data, va, corner_swb, corner_set, c_wall)
		end
		if edges.n then
			fill_vmanip_area(data, va, corner_nwb, corner_net, c_wall)
		end
		if edges.e then
			fill_vmanip_area(data, va, corner_seb, corner_net, c_wall)
		end
		if edges.w then
			fill_vmanip_area(data, va, corner_swb, corner_nwt, c_wall)
		end

		-- Inner Walls --
		if (bound_perlin(Perlin, 10, x, 5, z) > 3) and not place_opening(x) then
			fill_vmanip_area(data, va, v_new(x, 0, z), v_new(x, wall_height, z + 15), c_wall)
		end
		if (bound_perlin(Perlin, 10, x, 6, z) > 7) and not place_opening(z) then
			fill_vmanip_area(data, va, v_new(x, 0, z), v_new(x + 15, wall_height, z), c_wall)
		end

		-- Doorways --
		if place_opening(x) then
			if edges.s then
				if edges.inner.s then
					fill_vmanip_area(
						data,
						va,
						v_new(x + 3, 0, z + 0),
						v_new(x + 12, 4, z + 0),
						CONTENT_AIR
					)
				else
					fill_vmanip_area(
						data,
						va,
						v_new(x + 6, 0, z + 0),
						v_new(x + 9, 4, z + 0),
						CONTENT_AIR
					)
				end
			end
			if edges.n then
				if edges.inner.n then
					fill_vmanip_area(
						data,
						va,
						v_new(x + 3, 0, z + 15),
						v_new(x + 12, 4, z + 15),
						CONTENT_AIR
					)
				else
					fill_vmanip_area(
						data,
						va,
						v_new(x + 6, 0, z + 15),
						v_new(x + 9, 4, z + 15),
						CONTENT_AIR
					)
				end
			end
		end

		if place_opening(z) then
			if edges.e then
				if edges.inner.e then
					fill_vmanip_area(
						data,
						va,
						v_new(x + 15, 0, z + 1),
						v_new(x + 15, 4, z + 10),
						CONTENT_AIR
					)
				else
					fill_vmanip_area(
						data,
						va,
						v_new(x + 15, 0, z + 3),
						v_new(x + 15, 4, z + 6),
						CONTENT_AIR
					)
				end
			end
			if edges.w then
				if edges.inner.w then
					fill_vmanip_area(
						data,
						va,
						v_new(x + 0, 0, z + 1),
						v_new(x + 0, 4, z + 10),
						CONTENT_AIR
					)
				else
					fill_vmanip_area(
						data,
						va,
						v_new(x + 0, 0, z + 3),
						v_new(x + 0, 4, z + 6),
						CONTENT_AIR
					)
				end
			end
		end

		-- Furniture --
		if
			not (
				((edges.n or edges.s) and place_opening(x))
				or ((edges.e or edges.w) and place_opening(z))
			)
		then
			local room_name = rooms[bound_perlin(Perlin, #rooms, x, 0, z)]

			for x2 = x + 2, x + 13, 4 do
				for z2 = z + 2, z + 13, 4 do
					local display_id = bound_perlin(Perlin, #displays[room_name], x2, 1, z2)

					for _, v in ipairs(displays[room_name][display_id].nodes) do
						local groups = { ikea_furniture = 1, [v.type] = 1, [room_name] = 1 }
						local applicable_furniture = ikea.get_nodes_by_groups(groups)
						local id = bound_perlin(Perlin, #applicable_furniture, x + x2, 2, z + z2)
						local index = va:indexp(vector.add(v.pos, v_new(x2, 1, z2)))

						data[index] = minetest.get_content_id(applicable_furniture[id])
					end
				end
			end
			-- Trolleys --
		elseif bound_perlin(Perlin, 8, x, 10, z) == 1 then
			local index = va:index(x + 7, 1, z + 5)
			data[index] = c_trolley
			for i in va:iter(x + 7, 1, z + 6, x + 8, 1, z + 8) do
				data[i] = c_container
				param2s[i] = 2
			end
		end

		for i in va:iter(x, 0, z, x + 15, 0, z + 15) do
			data[i] = c_floor
		end
	end,
})
