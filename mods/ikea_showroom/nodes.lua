minetest.register_node("ikea_showroom:floor", {
	paramtype = "light",
	description = "Base Floor Node, Do Not Place (You Hacker!)",
	tiles = { { name = "ikea_showroom_concrete.png" } },
	groups = { static = 1 },
	sunlight_propagates = true,
})

minetest.register_node("ikea_showroom:wall", {
	paramtype = "light",
	description = "Base Wall Node, Do Not Place (You Hacker!)",
	tiles = { { name = "ikea_showroom_wall.png" } },
	groups = { static = 1 },
	sunlight_propagates = true,
})
