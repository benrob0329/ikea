local table_and_chairs = {
	name = "Table and Chairs",
	nodes = {
		{ pos = { x = 1, y = 0, z = 2 }, type = "chair" },
		{ pos = { x = 2, y = 0, z = 2 }, type = "chair" },
		{ pos = { x = 1, y = 0, z = 1 }, type = "table" },
	},
}

local sofa_and_chairs = {
	name = "Sofa and Chairs",
	nodes = {
		{ pos = { x = 1, y = 0, z = 2 }, type = "sofa" },
		{ pos = { x = 1, y = 0, z = 1 }, type = "table" },
	},
}

local bed = {
	name = "Bed",
	nodes = { { pos = { x = 2, y = 0, z = 1 }, type = "bed" } },
}

local rooms = { "kitchen", "lounge", "bedroom" }
local displays = {}
displays.bedroom = { bed }
displays.lounge = { table_and_chairs, sofa_and_chairs }
displays.kitchen = { table_and_chairs }

return rooms, displays
