local landskrona_nodebox = {
	type = "fixed",
	fixed = {
		{ 24 / 16, -6 / 16, 6 / 16, 22 / 16, 5 / 16, -8 / 16 }, -- Left Armrest
		{ -6 / 16, -6 / 16, 6 / 16, -8 / 16, 5 / 16, -8 / 16 }, -- Right Armrest
		{ 22 / 16, -3 / 16, 3 / 16, -6 / 16, 0 / 16, -9 / 16 }, -- Seat Cussion
		{ 22 / 16, 8 / 16, 3 / 16, -6 / 16, 0 / 16, 6 / 16 }, -- Back Cussion
		{ 24 / 16, -6 / 16, -8 / 16, -8 / 16, -3 / 16, -8 / 16 }, -- Bottom
		{ 24 / 16, -6 / 16, 6 / 16, -8 / 16, 5 / 16, 8 / 16 }, -- Back
	},
}

minetest.register_node(
	"ikea_furniture:landskrona",
	table.merge(ikea.default_mesh(), {
		description = "Landskrona Sofa (Blue)",
		mesh = "ikea_sofas_landskrona.obj",
		tiles = { "ikea_sofas_landskrona.png" },
		use_texture_alpha = "clip",
		selection_box = landskrona_nodebox,
		collision_box = landskrona_nodebox,
		groups = table.merge(ikea_furniture.default_groups(), { size_x = 2, sofa = 1, lounge = 1 }),
		tool_capabilities = {
			full_punch_interval = 8,
			damage_groups = { whacking = 20 },
		},
	})
)
