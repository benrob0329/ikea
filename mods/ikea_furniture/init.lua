ikea_furniture = {}

-- Default Nodedef Tables --
local error_node = minetest.registered_nodes["ikea:error"]

function ikea_furniture.default_groups()
	return { carryable = 1, ikea_furniture = 1, size_x = 1, size_y = 1, size_z = 1 }
end

function ikea_furniture.default_groups_rack()
	return { facsimile = 1, spawn_on_rack = 1 }
end

local modpath = minetest.get_modpath(minetest.get_current_modname()) .. "/"
dofile(modpath .. "chairs.lua")
dofile(modpath .. "tables.lua")
dofile(modpath .. "sofas.lua")
dofile(modpath .. "beds.lua")
dofile(modpath .. "nightstands.lua")
