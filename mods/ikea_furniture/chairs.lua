local vedbo_nodebox = {
	type = "fixed",
	fixed = {
		{ -7 / 16, -8 / 16, -7 / 16, 7 / 16, 2 / 16, 7 / 16 }, -- Bottom
		{ -7 / 16, 2 / 16, 6 / 16, 7 / 16, 16 / 16, 7 / 16 }, -- Back
		{ -7 / 16, 2 / 16, -7 / 16, -5 / 16, 4 / 16, 6 / 16 }, -- Right Armrest
		{ 5 / 16, 2 / 16, -7 / 16, 7 / 16, 4 / 16, 6 / 16 }, -- Left Armrest
	},
}

minetest.register_node(
	"ikea_furniture:vedbo",
	table.merge(ikea.default_mesh(), {
		description = "Vedbo Chair (Pink)",
		mesh = "ikea_chairs_vedbo.obj",
		tiles = { "ikea_chairs_vedbo.png" },
		use_texture_alpha = "clip",
		selection_box = vedbo_nodebox,
		collision_box = vedbo_nodebox,
		groups = table.merge(
			ikea_furniture.default_groups(),
			ikea_furniture.default_groups_rack(),
			{ chair = 1, lounge = 1 }
		),
		tool_capabilities = { full_punch_interval = 4, damage_groups = { fleshy = 10 } },
	})
)

local svenbertil_box = {
	type = "fixed",
	fixed = {
		{ -0.437484, 0.937491, 0.313884, 0.437516, 0.124991, 0.376384 }, -- Back
		{ -0.437484, 0.124991, -0.436116, 0.437516, 0.062491, 0.376384 }, -- Seat
		{ -0.374984, 0.062491, -0.373616, -0.312484, -0.500009, 0.313884 }, -- Left
		{ 0.312516, 0.062491, -0.373616, 0.375016, -0.500009, 0.313884 }, -- Right
	},
}

minetest.register_node(
	"ikea_furniture:svenbertil",
	table.merge(ikea.default_mesh(), {
		description = "Svenbertil Chair",
		mesh = "ikea_chairs_svenbertil.obj",
		tiles = { "ikea_chairs_svenbertil.png" },
		use_texture_alpha = "clip",
		selection_box = svenbertil_box,
		collision_box = svenbertil_box,
		groups = table.merge(
			ikea_furniture.default_groups(),
			ikea_furniture.default_groups_rack(),
			{ chair = 1, kitchen = 1 }
		),
		tool_capabilities = { full_punch_interval = 4, damage_groups = { fleshy = 10 } },
	})
)
