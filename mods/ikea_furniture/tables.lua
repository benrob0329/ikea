local listerby_box = {
	type = "fixed",
	fixed = {
		{ -0.25, 0.00125, -0.3125, 1.25, -0.06125, 0.3125 }, -- Tabletop
		{ -0.0625, -0.06125, -0.25, 0.0, -0.49875, 0.25 }, -- Left
		{ 1.0, -0.06125, -0.25, 1.0625, -0.49875, 0.25 }, -- Right
		{ 0.0, -0.31125, -0.25, 1.0, -0.37375, 0.25 }, -- Shelf
	},
}

minetest.register_node(
	"ikea_furniture:listerby",
	table.merge(ikea.default_mesh(), {
		description = "Listerby Table",
		mesh = "ikea_tables_listerby.obj",
		tiles = { "ikea_tables_listerby.png" },
		use_texture_alpha = "clip",
		selection_box = listerby_box,
		collision_box = listerby_box,
		groups = table.merge(
			ikea_furniture.default_groups(),
			{ size_x = 2, table = 1, lounge = 1 }
		),
		tool_capabilities = { full_punch_interval = 6, damage_groups = { fleshy = 15 } },
	})
)

local lisabo_box = {
	type = "fixed",
	fixed = {
		{ -0.625, 0.5, -0.5625, 1.625, 0.4375, 0.5625 }, -- Tabletop
		{ -0.5625, 0.4375, -0.5, 1.5625, 0.375, 0.5 }, -- Lower Tabletop
		{ 1.4375, 0.375, -0.4375, 1.5, -0.5, -0.375 }, -- Front Right Leg
		{ 1.4375, 0.375, 0.375, 1.5, -0.5, 0.4375 }, -- Back Right Leg
		{ -0.5, 0.375, -0.4375, -0.4375, -0.5, -0.375 }, -- Front Left Leg
		{ -0.5, 0.375, 0.375, -0.4375, -0.5, 0.4375 }, -- Back Left Leg
	},
}

minetest.register_node(
	"ikea_furniture:lisabo",
	table.merge(ikea.default_mesh(), {
		description = "Lisabo Table",
		mesh = "ikea_tables_lisabo.obj",
		tiles = { "ikea_tables_lisabo.png" },
		selection_box = lisabo_box,
		collision_box = lisabo_box,
		groups = table.merge(
			ikea_furniture.default_groups(),
			{ size_x = 2, table = 1, kitchen = 1 }
		),
		tool_capabilities = { full_punch_interval = 6, damage_groups = { fleshy = 15 } },
	})
)
