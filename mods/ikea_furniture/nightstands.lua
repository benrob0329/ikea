local hemnes_box = {
	type = "fixed",
	fixed = { { -0.25, 0.25, -0.1875, 0.25, -0.5, 0.1875 } },
}

minetest.register_node(
	"ikea_furniture:hemnes",
	table.merge(ikea.default_mesh(), {
		description = "Hemnes Nightstand",
		mesh = "ikea_nightstands_hemnes.obj",
		tiles = { "ikea_nightstands_hemnes.png" },
		use_texture_alpha = "clip",
		selection_box = hemnes_box,
		collision_box = hemnes_box,
		groups = table.merge(
			ikea_furniture.default_groups(),
			ikea_furniture.default_groups_rack(),
			{ nightstand = 1, bedroom = 1 }
		),
		tool_capabilities = { full_punch_interval = 1, damage_groups = { fleshy = 5 } },
	})
)
