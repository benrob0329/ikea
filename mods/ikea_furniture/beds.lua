local malm_box = {
	type = "fixed",
	fixed = {
		{ -0.875, 0.5, 1.4375, 0.875, -0.5, 1.5 }, -- Headboard
		{ -0.875, 0.0, 1.4375, 0.8125, -0.5, -0.6875 }, -- Everything Else
	},
}

minetest.register_node(
	"ikea_furniture:malm",
	table.merge(ikea.default_mesh(), {
		description = "Malm Bed",
		mesh = "ikea_beds_malm.obj",
		tiles = { "ikea_beds_malm.png" },
		use_texture_alpha = "clip",
		selection_box = malm_box,
		collision_box = malm_box,
		groups = table.merge(
			ikea_furniture.default_groups(),
			{ size_x = 2, size_z = 2, bed = 1, bedroom = 1 }
		),
		tool_capabilities = {
			full_punch_interval = 15,
			damage_groups = { fleshy = 30 },
		},
	})
)
