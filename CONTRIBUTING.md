# Rules For Contributing To This Project
By submitting additions or changes of any form to this project, you agree to abide by the following rules:

* All submissions are solely those which you have the adequate legal rights and/or ownership of to abide by the rules of this document
* All submissions are to be licensed consistently with the rest of this project unless deemed necessary by a project maintainer
* You must add a `Portions Copyright (C) <year> <name>` entree to the LICENSE file (or append to an existing entree) which contains your name (or a pseudonym), the year of your contribution's creation (or a range of years if applicable), and optionally your email address for ease of contact

# Source Code Formatting Guidelines
All source code submitted to this project should be formatted using [StyLua](https://github.com/JohnnyMorganz/StyLua) before committing.

# Texture Creation Guidelines
All textures submitted to this project are to abide by the following guidelines whenever applicable, unless waived by a project maintainer:

* All colors should come from GIMP's "Caramel" palette, if extra colors are needed only the brightness or only the hue of one of these colors should be changed
* All textures should be at the same scale as a standard 16x16 node texture
* Any source files for the textures should be included under a `src` directory inside the `media` folder inside the mod, or inside of an appropriate subfolder within

# Model Creation Guidelines
All models submitted to this project are to abide by the following guidelines whenever applicable, unless waived by a project maintainer:

* All models should be made to a style consisting of cuboids 1/16th of a meter in size
* Static models (those not animated) should be exported to an OBJ file
* Animated models should be exported to a B3D file
* The source files for the models should be included under a `src` directory inside the `media` folder inside the mod
