ignore = {"212", "542"}
read_globals = {
	"minetest",
	"VoxelArea",
	"vector",
	"Raycast",
	"ItemStack",
	"PerlinNoise",
	"dump",
	"mobkit",
	"l",
	"DEBUG",
	"ikea",
	"ikea_mapgen",
	"ikea_facsimile",
}

read_globals.table = {
	fields = {
		"copy",
		"equals",
		"shuffle",
		"merge",
	}
}

read_globals.string = {
	fields = {
		"split",
	}
}

read_globals.vector = {
		fields = {
		"dot",
	}
}

exclude_files = {"mods/mobkit", "mods/lambda", "mods/ikea_staff/behaviors2override.lua"}

files["mods/ikea/init.lua"] = {globals = {"DEBUG", "ikea", "table"}}
files["mods/ikea_mapgen/init.lua"] = {globals = {"ikea_mapgen"}}
files["mods/ikea_facsimile/init.lua"] = {globals = {"ikea_facsimile"}}

